# Netamqp

An AMQP client.

 - [Project page](http://projects.camlcity.org/projects/netamqp.html)

 - [README](README)

 - [ChangeLog (master)](ChangeLog)

 - [Installation](INSTALL)

 - [License](LICENSE)

